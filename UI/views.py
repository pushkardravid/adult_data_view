from django.shortcuts import render

def index(request):

    context_dict = {'boldmessage': "I am bold font from the context"}

    return render(request, 'UI/index.html', context_dict)